package arvorefinal;

public class NoArvore {
	String nome;
	int	valor;
	NoArvore direita;
	NoArvore esquerda;
	
    public NoArvore(){
        this.valor = 0;
        this.direita = null;
        this.esquerda = null;
        this.nome = "";
    }
    
    public NoArvore(int novovalor, String nome){
        this.valor = novovalor;
        this.direita = null;
        this.esquerda = null;
        this.nome = nome;
    }

	public NoArvore busca(NoArvore no, int valorprocurado, String nome) {
		if(no == null)
			return null;
		
		if(no.valor > valorprocurado)
			return busca(no.esquerda, valorprocurado, nome);
		else if(no.valor < valorprocurado)
			return busca(no.direita, valorprocurado, nome);
		else
		return no;
	}
	
	public NoArvore insere(NoArvore no, int novovalor, String nome) {
		if(no == null) {
			no = new NoArvore();
			no.valor = novovalor;
			no.esquerda = no.direita = null;
			no.nome = nome;
		}
		else if(novovalor < no.valor)
			no.esquerda = insere(no.esquerda, novovalor, nome);
		else if(novovalor > no.valor)
			no.direita = insere(no.direita, novovalor, nome);
	
		return no;
	}

	public NoArvore remove(NoArvore raiz, int valoraremover) {
		// faz a busca pelo valor a ser removido
		if (raiz == null)
			return null;
		else if (raiz.valor > valoraremover)
			raiz.esquerda = remove(raiz.esquerda, valoraremover);
		else if (raiz.valor < valoraremover)
			raiz.direita = remove(raiz.direita, valoraremover);
		else {	// passar por aqui significa que achou o n� com o
				// valor a remover procurado e agora vai remove-lo
				// segundo as 4 situa��eses a seguir:
			// N�o ter filhos (esquerda e direita == null)
			if (raiz.esquerda == null && raiz.direita == null) {
				raiz = null;
			}
			// Ter filho apenas � direita (esquerda == null)
			else if (raiz. esquerda == null) {
				raiz = raiz.direita;
			}
			// Ter filho apenas � esquerda (direita == null)
			else if (raiz.direita == null) {
				raiz = raiz.esquerda;
			}
			// Ter dois filhos (esquerda e direita != null)
			else {
				NoArvore sub_esquerda = raiz.esquerda;
				// encontrar o n� com maior valor na sub�rvore esquerda
				while (sub_esquerda.direita != null) {
					sub_esquerda = sub_esquerda.direita;
				}
				// aqui a sub_esquerda.valor tem o maior valor
				raiz.valor = sub_esquerda.valor;
				sub_esquerda.valor = valoraremover;
				raiz.esquerda = remove(raiz.esquerda, valoraremover);
			}
		}
		return raiz;		
	}
	

	public void imprimeIn(NoArvore raiz) {
		if (raiz != null) {
			imprimeIn(raiz.esquerda);
			System.out.print("|RGM: " +raiz.valor+ "| ");
			imprimeIn(raiz.direita);
		}
	}
	
	public void imprimePos(NoArvore raiz) {
		if (raiz != null) {
			imprimePos(raiz.esquerda);
			imprimePos(raiz.direita);
			System.out.print("|RGM: " +raiz.valor+ "| ");
		}
	}
	
	public void imprimePre(NoArvore raiz) {
		if (raiz != null) {
			System.out.print("|RGM: " +raiz.valor+ "| ");
			imprimePre(raiz.esquerda);
			imprimePre(raiz.direita);
		}
	}
	public void caminhar(NoArvore raiz) {
		if(raiz != null) {
			System.out.print("\n Exibindo em ordem: ");
			imprimeIn(raiz);
			System.out.print("\n Exibindo em pos-ordem: ");
			imprimePos(raiz);
			System.out.print("\n Exibindo em pre-ordem: ");
			imprimePre(raiz);
			System.out.print("\n");
		}

	}
	
	public void limparArvore() {
		this.valor = 0;
		this.nome = "";
		this.direita = null;
		this.esquerda = null;
	}

}
